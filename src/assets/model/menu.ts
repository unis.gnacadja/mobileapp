export const menu = [
    {

        cat: 'Dejeuner',

        repas: [

            {
                id: '1',
                titre: 'L’attiéké',
                description: 'Plat traditionnel ivoirien',
                prix: '2000',
                image: 'https://i1.wp.com/afriquetimes.com/wp-content/uploads/2016/12/02-2.jpg?resize=696%2C696&ssl=1',
                ingredients: [
                    {
                        ingredient: 'ingrédient 1'
                    },
                    {
                        ingredient: 'ingrédient 2'
                    },
                    {
                        ingredient: 'ingrédient 3'
                    },
                    {
                        ingredient: 'ingrédient 4'
                    },
                    {
                        ingredient: 'ingrédient 5'
                    }


                ]
            },

            {
                id: '2',
                titre: 'Allocos',
                description: 'bananes plantains fries',
                prix: '3000',
                image: 'https://i0.wp.com/afriquetimes.com/wp-content/uploads/2016/12/09.jpg?resize=696%2C696&ssl=1',
                ingredients: [
                    {
                        ingredient: 'ingrédient 6'
                    },
                    {
                        ingredient: 'ingrédient 7'
                    },
                    {
                        ingredient: 'ingrédient 8'
                    },
                    {
                        ingredient: 'ingrédient 9'
                    },
                    {
                        ingredient: 'ingrédient 10'
                    }


                ]
            },
            {
                id: '3',
                titre: 'Le couscous',
                description: 'le plus connu d’Afrique du Nord',
                prix: '1000',
                image: 'https://i0.wp.com/afriquetimes.com/wp-content/uploads/2016/12/11.jpg?resize=696%2C696&ssl=1',
                ingredients: [
                    {
                        ingredient: 'ingrédient 11'
                    },
                    {
                        ingredient: 'ingrédient 12'
                    },
                    {
                        ingredient: 'ingrédient 13'
                    },
                    {
                        ingredient: 'ingrédient 14'
                    },
                    {
                        ingredient: 'ingrédient 15'
                    }


                ]
            },
            {
                id: '4',
                titre: ' Le poulet yassa',
                description: 'Poulet, oignons, accompagné d’un riz',
                prix: '5000',
                image: 'https://i0.wp.com/afriquetimes.com/wp-content/uploads/2016/12/10.jpg?resize=696%2C869&ssl=1',
                ingredients: [
                    {
                        ingredient: 'ingrédient 16'
                    },
                    {
                        ingredient: 'ingrédient 17'
                    },
                    {
                        ingredient: 'ingrédient 18'
                    },
                    {
                        ingredient: 'ingrédient 19'
                    },
                    {
                        ingredient: 'ingrédient 20'
                    }


                ]
            },
            {
                id: '5',
                titre: 'Sauce gombo',
                description: 'Sauce est particulièrement gluante',
                prix: '2000',
                image: 'https://i1.wp.com/afriquetimes.com/wp-content/uploads/2016/12/19.jpg?resize=696%2C696&ssl=1',
                ingredients: [
                    {
                        ingredient: 'ingrédient 21'
                    },
                    {
                        ingredient: 'ingrédient 22'
                    },
                    {
                        ingredient: 'ingrédient 23'
                    },
                    {
                        ingredient: 'ingrédient 24'
                    },
                    {
                        ingredient: 'ingrédient 25'
                    }


                ]
            }

        ]

    },
    {
        cat: 'Café et Sandwich',
        repas: [
            {
                id: '6',
                titre: 'Cappuccino',
                description: 'Un espresso et du lait chaud',
                prix: '500',
                image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/60/Cappuccino_with_foam.jpg/150px-Cappuccino_with_foam.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 26'
                    },
                    {
                        ingredient: 'ingrédient 27'
                    },
                    {
                        ingredient: 'ingrédient 28'
                    },
                    {
                        ingredient: 'ingrédient 29'
                    },
                    {
                        ingredient: 'ingrédient 30'
                    }


                ]
            },
            {
                id: '7',
                titre: 'Espresso',
                description: ' L\'expresso est extrait par pression',
                prix: '1500',
                image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d4/%28A_Donde_Vamos%2C_Quito%29_Chocolate_and_Espresso.JPG/150px-%28A_Donde_Vamos%2C_Quito%29_Chocolate_and_Espresso.JPG',
                ingredients: [
                    {
                        ingredient: 'ingrédient 31'
                    },
                    {
                        ingredient: 'ingrédient 32'
                    },
                    {
                        ingredient: 'ingrédient 33'
                    },
                    {
                        ingredient: 'ingrédient 34'
                    },
                    {
                        ingredient: 'ingrédient 35'
                    }


                ]
            },
            {
                id: '8',
                titre: 'Flat white',
                description: 'Café au lait comme un café latte',
                prix: '1000',
                image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/Flat_White_Coffee.png/150px-Flat_White_Coffee.png',
                ingredients: [
                    {
                        ingredient: 'ingrédient 36'
                    },
                    {
                        ingredient: 'ingrédient 37'
                    },
                    {
                        ingredient: 'ingrédient 38'
                    },
                    {
                        ingredient: 'ingrédient 39'
                    },
                    {
                        ingredient: 'ingrédient 40'
                    }


                ]
            },
            {
                id: '9',
                titre: 'Affogato',
                description: ' Une boisson ou un dessert avec un expresso',
                prix: '1000',
                image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/19/Affogato.JPG/150px-Affogato.JPG',
                ingredients: [
                    {
                        ingredient: 'ingrédient 41'
                    },
                    {
                        ingredient: 'ingrédient 42'
                    },
                    {
                        ingredient: 'ingrédient 43'
                    },
                    {
                        ingredient: 'ingrédient 44'
                    },
                    {
                        ingredient: 'ingrédient 45'
                    }


                ]
            },
            {
                id: '10',
                titre: 'Macchiato',
                description: 'Une mousse de lait chaud fouetté',
                prix: '500',
                image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Macchiato_FourBarrel.jpg/150px-Macchiato_FourBarrel.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 46'
                    },
                    {
                        ingredient: 'ingrédient 47'
                    },
                    {
                        ingredient: 'ingrédient 48'
                    },
                    {
                        ingredient: 'ingrédient 49'
                    },
                    {
                        ingredient: 'ingrédient 50'
                    }


                ]
            }

        ]

    },
    {
        cat: 'Snack ',
        repas: [
            {
                id: '11',
                titre: 'Martini',
                description: 'Martini, shaken, not stirred ',
                prix: '2500',
                image: 'https://boiremixologie.com/files/medias/_thumb/capture-ecran-martini.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 51'
                    },
                    {
                        ingredient: 'ingrédient 52'
                    },
                    {
                        ingredient: 'ingrédient 53'
                    },
                    {
                        ingredient: 'ingrédient 54'
                    },
                    {
                        ingredient: 'ingrédient 55'
                    }


                ]
            },
            {
                id: '12',
                titre: 'Margarita',
                description: 'cocktail rafraichissant à base de tequila',
                prix: '1500',
                image: 'https://boiremixologie.com/files/medias/_thumb/margarita.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 56'
                    },
                    {
                        ingredient: 'ingrédient 57'
                    },
                    {
                        ingredient: 'ingrédient 58'
                    },
                    {
                        ingredient: 'ingrédient 59'
                    },
                    {
                        ingredient: 'ingrédient 60'
                    }


                ]
            },
            {
                id: '13',
                titre: 'Mojito',
                description: 'Jus de cannes à sucre au rhum',
                prix: '500',
                image: 'https://boiremixologie.com/files/medias/_thumb/mojito.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 61'
                    },
                    {
                        ingredient: 'ingrédient 62'
                    },
                    {
                        ingredient: 'ingrédient 63'
                    },
                    {
                        ingredient: 'ingrédient 64'
                    },
                    {
                        ingredient: 'ingrédient 65'
                    }


                ]
            },
            {
                id: '14',
                titre: 'Cosmopolitan',
                description: 'L’incontournable cocktail a été popularisé',
                prix: '2000',
                image: 'https://boiremixologie.com/files/medias/_thumb/cocktail-cosmopolitain-boire.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 66'
                    },
                    {
                        ingredient: 'ingrédient 67'
                    },
                    {
                        ingredient: 'ingrédient 68'
                    },
                    {
                        ingredient: 'ingrédient 69'
                    },
                    {
                        ingredient: 'ingrédient 70'
                    }


                ]
            },
            {
                id: '15',
                titre: 'Negroni',
                description: 'Un cocktail, trois ingrédients',
                prix: '1000',
                image: 'https://boiremixologie.com/files/medias/_thumb/Cocktail-gin-vermouth-campari.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 71'
                    },
                    {
                        ingredient: 'ingrédient 72'
                    },
                    {
                        ingredient: 'ingrédient 73'
                    },
                    {
                        ingredient: 'ingrédient 74'
                    },
                    {
                        ingredient: 'ingrédient 75'
                    }


                ]
            },

        ]
    },
    {
        cat: 'Patisseries',
        repas: [
            {
                id: '16',
                titre: 'Paris-Brest',
                description: 'La roue de vélo la plus gourmande',
                prix: '5000',
                image: 'https://www.ptitchef.com/imgupl/feed-data/md-1568822.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 76'
                    },
                    {
                        ingredient: 'ingrédient 77'
                    },
                    {
                        ingredient: 'ingrédient 78'
                    },
                    {
                        ingredient: 'ingrédient 79'
                    },
                    {
                        ingredient: 'ingrédient 80'
                    }


                ]
            },
            {
                id: '17',
                titre: 'Tropézienne',
                description: 'Pâtissierie d\'origine polonaise',
                prix: '3000',
                image: 'https://www.ptitchef.com/imgupl/feed-data/md-1562761.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 81'
                    },
                    {
                        ingredient: 'ingrédient 82'
                    },
                    {
                        ingredient: 'ingrédient 83'
                    },
                    {
                        ingredient: 'ingrédient 84'
                    },
                    {
                        ingredient: 'ingrédient 85'
                    }


                ]
            },
            {
                id: '18',
                titre: 'Profiteroles',
                description: 'choux collés avec du caramel',
                prix: '2000',
                image: 'https://www.ptitchef.com/imgupl/feed-data/md-1390752.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 86'
                    },
                    {
                        ingredient: 'ingrédient 87'
                    },
                    {
                        ingredient: 'ingrédient 88'
                    },
                    {
                        ingredient: 'ingrédient 89'
                    },
                    {
                        ingredient: 'ingrédient 90'
                    }


                ]
            },
            {
                id: '19',
                titre: 'Saint-Honoré',
                description: ' la pâte à choux !',
                prix: '5000',
                image: 'https://www.ptitchef.com/imgupl/feed-data/md-1567079.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 91'
                    },
                    {
                        ingredient: 'ingrédient 92'
                    },
                    {
                        ingredient: 'ingrédient 93'
                    },
                    {
                        ingredient: 'ingrédient 94'
                    },
                    {
                        ingredient: 'ingrédient 95'
                    }


                ]
            },
            {

                id: '20',
                titre: 'Mille feuille',
                description: 'Trois couches de pâte feuilletée',
                prix: '7000',
                image: 'https://www.ptitchef.com/imgupl/feed-data/md-1567489.jpg',
                ingredients: [
                    {
                        ingredient: 'ingrédient 96'
                    },
                    {
                        ingredient: 'ingrédient 97'
                    },
                    {
                        ingredient: 'ingrédient 98'
                    },
                    {
                        ingredient: 'ingrédient 99'
                    },
                    {
                        ingredient: 'ingrédient 100'
                    }


                ]
            }

        ]
    }


]
