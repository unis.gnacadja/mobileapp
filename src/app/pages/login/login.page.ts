import { Component, OnInit } from '@angular/core';
import {
  NavController
  // NavParams
} from '@ionic/angular';
import { FieldValidatorService } from './../../provider/field-validator/field-validator.service';
import { Storage } from '@ionic/storage';
import { RouterModule } from '@angular/router';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user = {
    name: '',
    number: '',
  };
  numberUser: any;
  savemessage: any;
  isEnabled: boolean;
  disable: boolean;
  number: any;
  warning = '';
  message = '';


  constructor(
    public navCtrl: NavController,
    public router: RouterModule,
    public fieldValidatorProvider: FieldValidatorService,
    private localStorage: Storage
  ) { }

  ngOnInit() {
  }

  ionViewDidLoad() { }

  validateNumbers() {
    const a = this.fieldValidatorProvider.validateNumber(this.user.number);
    this.warning = a.info;
    this.isEnabled = a.state;
  }

  checkIsEnableds() {
    const el = this.fieldValidatorProvider.checkIsEnabled(this.isEnabled);
    return el;
  }

  saveNumber() {
    this.localStorage.set(environment.values.USER_PHONE, this.user.number);
    this.navCtrl.navigateRoot('/user-home');
  }

  goVerifyPage() {
    this.localStorage.set('MyNumber', this.user.number);
    this.navCtrl.navigateForward('/verify-number');
  }

}
