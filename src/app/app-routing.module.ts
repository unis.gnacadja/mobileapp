import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  // {
  //   path: 'add-card',
  //   loadChildren: () => import('./pages/add-card/add-card.module').then(m => m.AddCardPageModule)
  // },
  // {
  //   path: 'checkout',
  //   loadChildren: () => import('./pages/checkout/checkout.module').then(m => m.CheckoutPageModule)
  // },
  // {
  //   path: 'filters',
  //   loadChildren: () => import('./pages/filters/filters.module').then(m => m.FiltersPageModule)
  // },
  // {
  //   path: 'new-address',
  //   loadChildren: () => import('./pages/new-address/new-address.module').then(m => m.NewAddressPageModule)
  // },
  // {
  //   path: 'product-list',
  //   loadChildren: () => import('./pages/product-list/product-list.module').then(m => m.ProductListPageModule)
  // },
  {
    path: 'sliders',
    loadChildren: () => import('./pages/sliders/sliders.module').then(m => m.SlidersPageModule)
  },
  // {
  //   path: 'user-profile',
  //   loadChildren: () => import('./pages/user-profile/user-profile.module').then(m => m.UserProfilePageModule)
  // },
  {
    path: '',
    redirectTo: 'sliders',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
