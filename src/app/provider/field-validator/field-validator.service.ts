import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FieldValidatorService {

  constructor() { }

  checkIsEnabled(value: boolean) { return !value; }

  validateNumber(value1) {

    return {
      state: !(value1.length < 8 || /[^0-9]/.test(value1)),
      info: (value1.length === 8 && /[^0-9]/.test(value1)) ? 'Numéro invalide.' : ''
    };

  }
}
