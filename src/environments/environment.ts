// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  facture: {
    coutlivraison: '250',
  },

  adress: {
    adress: '00251548',
  },
  values: {
    CORDOVA_PLATEFORM: 'cordova',
    USER_PHONE: 'MyNumber',
    SDR_MENU: 'listMenu',
    APP_NAME: 'SDR',
    USER_CART: 'panie'
  },
  api: {
    basePath: 'http://sdr.api.rintio.com/', // "http://192.168.43.101/sdr_api/api/",
    menuEndpoint: 'sf/index.php?rquest=getmenu',
    commandeEndpoint: 'sf/index.php?rquest=commander',
    errorEndpoint: 'sf/index.php?rquest=error',
  },

  message: {
    MENU_LOADING: 'Chargement du menu',
    erreur: 'Service indisponible',
    SERVICE_UNAVAILABLE: 'Impossible de joindre le serveur Vérifier votre connexion'
  },

  image: {
    url: 'http://cdn.ressources.rintio.com/sdr/logo.png',
    description: 'Aucune description',
  },

  livraison: {
    message:
      '*Notre service de livraison est disponible de 8h à 18h. Merci pour votre fidélité!',
    lieulivraison:
      'Service de livraison disponible sur Cotonou, Godomey et Calavi. ',
    heureouverture: '*Nous sommes ouverts de 8h à 22h ',
  },
  OneSignal: {
    signal_app_id: '3895a983-fe9d-4d23-9464-76b630c49d13',
    firebase_id: '429880702837',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
